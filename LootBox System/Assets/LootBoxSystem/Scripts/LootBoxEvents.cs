﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootBoxEvents : MonoBehaviour
{

    public GameObject[] LootItems;
    public static int items;
    public static int i { get; set; }

    private int color;

    void Start()
    {
        items = LootItems.Length - 1; // Array -1 gives the value of its total index

    }

    // Update is called once per frame
    void Update()
    {

    }


    public void OnEvent() // When UI button gets clicked
    {
        LootItems[i].SetActive(true); // turns on GO according to the value received from 

        print(LootItems[i]);
    }
}

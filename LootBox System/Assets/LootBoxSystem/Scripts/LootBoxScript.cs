﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootBoxScript :LootBoxEvents {
    //Script Handles box animation and both box and weapon spawn particle systems

    public GameObject LootBox;
    public Transform GO_Spawn;
    public AudioSource FX;
    public static int ItemIndex;
    public List<GameObject> usedLootBox;
  //  private Animator anim;

	// Use this for initialization
	void Start () {
        FX = gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        


    }


    public void OnClick() // When UI button gets clicked
    {
        FX.Play();
        if (usedLootBox.Count != 0) { // Destroys previous gameobject and removes it from list
            Destroy(usedLootBox[0]);
            usedLootBox.Remove(usedLootBox[0]);
            SpawnItem();
        } else SpawnItem(); // if list is already 0 Spawn

    }

    private void SpawnItem()
    {
        
        GameObject clone = Instantiate(LootBox, GO_Spawn.position, GO_Spawn.rotation) as GameObject; // Instantiates out lootbox
        usedLootBox.Add(clone); // adds instantiated box to list
        Animator anim = clone.GetComponent<Animator>();
        anim.Play("ShatterAnim");
        ItemIndex = Extensions.Odds(0, items, 3); // gets a random value for our items array, with odds favoring the common
        i = ItemIndex; // sets i in parent script to item index
    }

}
